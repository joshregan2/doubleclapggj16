package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;
import flixel.addons.text.FlxTypeText;

class TextScreenDisplay extends FlxTypedSpriteGroup<FlxSprite>
{
  public static inline var PANEL_WIDTH:Int = 268;
  public static inline var PANEL_HEIGHT:Int = 162;

  public var screenSprite:FlxSprite;
  public var screenText:FlxTypeText;

	public function new(X:Int, Y:Int, text:String)
	{
		super(X, Y);

    var paddingX:Int = 24;
    var paddingY:Int = 32;

    screenSprite = new FlxSprite(0,0);
    screenSprite.loadGraphic(Reg.IMG_TEXT_SCREEN, PANEL_WIDTH, PANEL_HEIGHT);
    add(screenSprite);

    screenText = new FlxTypeText(paddingX, paddingY, PANEL_WIDTH-(paddingX*2), text);
    screenText.text = text;
    screenText.setFormat(Reg.DEFAULT_FONT, 24, Reg.DEFAULT_FONT_COLOUR, "left");
    add(screenText);

    changeText(text);
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{

    super.update();
	}

  public function changeText(newText:String):Void
  {
    screenText.resetText(newText);
    screenText.start(0.05);
  }
}
