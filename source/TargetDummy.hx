package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class TargetDummy extends FlxSprite
{
  public static inline var TARGET_DUMMY_WIDTH:Int = 150;
  public static inline var TARGET_DUMMY_HEIGHT:Int = 150;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_TARGET_DUMMY_SPRITESHEET, true, TARGET_DUMMY_WIDTH, TARGET_DUMMY_HEIGHT);
    immovable = true;

    setSize(100, 100);

    var fRate:Int = 12;

    var idleFrames:Array<Int> = new Array<Int>();
    for (i in 0...23)
    {
      idleFrames.push(i);
    }
    animation.add("idle", idleFrames, fRate, true);

    var hitFrames:Array<Int> = new Array<Int>();
    for (i in 24...29)
    {
      hitFrames.push(i);
    }
    animation.add("hit", hitFrames, fRate, false);

    var brokenFrames:Array<Int> = new Array<Int>();
    for (i in 29...55)
    {
      brokenFrames.push(i);
    }
    animation.add("broken", brokenFrames, fRate, false);

    animation.play("idle");
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    super.update();
	}
}
