package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class FirstRitualState extends RitualState
{
	var hintTextScreen:TextScreenDisplay;
	var hintIdx:Int = 0;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		if (player.x >= FlxG.width)
		{
			resetRitualState();
		}
		else if (player.x <= -Player.PLAYER_BOUNDS_WIDTH)
		{
			didSucceed = true;
		}

		super.update();
	}

  override public function setupRitualScene():Void
  {
		backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    add(backgroundSprite);

    pipesSprite = new FlxSprite(0,0);
    pipesSprite.loadGraphic(Reg.IMG_PIPES_STRING, FlxG.width,FlxG.height);
    add(pipesSprite);

		setupLevelTilemap("Room1.tmx", "Tileset.png");

		hintTextScreen = new TextScreenDisplay(368,
		320, "Welcome to R.I.T.U.A.L, this is your 53423rd session in the hero training simulator.");
    add(hintTextScreen);

		var arrowTextScreenLeft:TextScreenDisplay = new TextScreenDisplay(68,
		320, "==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>");
		add(arrowTextScreenLeft);

		var arrowTextScreenRight:TextScreenDisplay = new TextScreenDisplay(668,
		320, "==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>  ==>");
		add(arrowTextScreenRight);

		setupPlayerAtPosition(490,FlxG.height-(192+Player.PLAYER_BOUNDS_HEIGHT), true);

		setupRitualEyeAtPosition(442, 220);

		super.setupRitualScene();
  }

	override public function resetRitualState():Void
	{
		player.x -= FlxG.width;
		hintIdx++;

		var idxMod:Int = hintIdx % 3;
		if (idxMod == 1)
		{
			hintTextScreen.changeText("Great keep going");
		}
		else if (idxMod == 2)
		{
			hintTextScreen.changeText("Don't Stop! Keep Going");
		}
		else if (idxMod == 3)
		{
			hintTextScreen.changeText( "Keep going right and nothing bad will happen.");
		}
		else
		{
			hintTextScreen.changeText("The princess is in trouble [runner name here]! Save her by walking right.");
		}

	}

	override public function goNextRitualState(Void):Void
	{
		FlxG.switchState(new SecondRitualState());
	}
}
