package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class PressurePlate extends FlxSprite
{
  public static inline var PRESSURE_PLATE_WIDTH:Int = 64;
  public static inline var PRESSURE_PLATE_HEIGHT:Int = 10;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_PRESSURE_PLATE, true, PRESSURE_PLATE_WIDTH, PRESSURE_PLATE_HEIGHT);

    animation.add("normal", [0]);
    animation.add("pressed", [1]);

    changeToNormal();
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    super.update();
	}

  public function changeToNormal():Void
  {
    animation.play("normal");
  }

  public function changeToPressed():Void
  {
    animation.play("pressed");
  }

}
