package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class BigDoorBack extends FlxSprite
{
  public static inline var DOOR_BACK_WIDTH:Int = 153;
  public static inline var DOOR_BACK_HEIGHT:Int = 316;
  public static inline var DOOR_FRONT_WIDTH:Int = 177;
  public static inline var DOOR_FRONT_HEIGHT:Int = 316;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_DOOR_BACK_SPRITESHEET,DOOR_BACK_WIDTH,DOOR_BACK_HEIGHT);

    animation.add("close", [2,1,0], 12, false);
    animation.add("open", [0,1,2], 12, false);

    immovable = true;
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{

    super.update();
	}

  public function open():Void
  {
    animation.play("open");
  }

  public function close():Void
  {
    animation.play("close");
  }
}
