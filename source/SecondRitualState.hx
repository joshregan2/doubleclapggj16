package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class SecondRitualState extends RitualState
{
	private var hintTextScreen:TextScreenDisplay;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		if (player.x <= -Player.PLAYER_BOUNDS_WIDTH)
		{
			resetRitualState();
		}

    if (player.y >= FlxG.height)
		{
			didSucceed = true;
		}

		super.update();
	}

  override public function setupRitualScene():Void
  {
		backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    add(backgroundSprite);

    pipesSprite = new FlxSprite(0,0);
    pipesSprite.loadGraphic(Reg.IMG_PIPES_STRING, FlxG.width,FlxG.height);
    add(pipesSprite);

    var pillar1 = new FlxSprite(64, 0);
    pillar1.loadGraphic(Reg.IMG_PILLAR_1, 199, 768);
    add(pillar1);

    var pillar2 = new FlxSprite(768, 0);
    pillar2.loadGraphic(Reg.IMG_PILLAR_2, 199, 768);
    add(pillar2);

		var light1 = new AnimatedLight(128, 180);
		add(light1);

		var light2 = new AnimatedLight(832, 180);
		add(light2);


    setupLevelTilemap("Room2.tmx", "Tileset.png");

    hintTextScreen = new TextScreenDisplay(360,
    240, "Jump Training. YOu jUmp. I sAy hoW hIGh. Hero.");
    add(hintTextScreen);

    setupPlayerAtPosition(FlxG.width - 32,FlxG.height-(256+Player.PLAYER_BOUNDS_HEIGHT), false);

    setupRitualEyeAtPosition(442, 128);

    super.setupRitualScene();
  }

	override public function resetRitualState():Void
	{
		player.x += FlxG.width;
	}

	override public function goNextRitualState(Void):Void
	{
		FlxG.switchState(new ThirdRitualState());
	}
}
