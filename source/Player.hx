package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Player extends FlxSprite
{
  public static inline var PLAYER_WIDTH:Int = 122;
  public static inline var PLAYER_HEIGHT:Int = 129;

  public static inline var PLAYER_BOUNDS_WIDTH:Int = 60;
  public static inline var PLAYER_BOUNDS_HEIGHT:Int = 129;

  public static inline var PLAYER_SPEED_X:Int = 1000;
  public static inline var PLAYER_DRAG_X:Int = 800;

  public static inline var PLAYER_JUMP:Int = 800;

  public var touchingFloor:Bool;

  public var didAttack:Bool = false;

  public var isDead:Bool = false;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_PLAYER_SPRITESHEET, true, PLAYER_WIDTH, PLAYER_HEIGHT);

    setSize(60, PLAYER_HEIGHT);
		offset.set(26, 0);

		drag.x = PLAYER_DRAG_X;
		acceleration.y = Reg.DEFAULT_GRAVITY;
		maxVelocity.set(320, 1000);

    touchingFloor = false;

    animation.add("idle", [0]);
		animation.add("run", [1, 2, 3, 4], 10);
    animation.add("jump", [5]);
    animation.add("fall", [6]);
    animation.add("attack", [8, 9, 10], 12, false);
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    if (didAttack == false)
    {
  		if (touchingFloor == false && velocity.y < 0)
  		{
  			animation.play("jump");
  		}
  		else if (velocity.x == 0)
  		{
  			animation.play("idle");
  		}
  		else
  		{
  			animation.play("run");
  		}

      if (touchingFloor == false && velocity.y > 0)
      {
        animation.play("fall");
      }
    }

    if (didAttack == true &&
        animation.finished == true)
    {
      didAttack = false;
    }

    super.update();
	}

	// MOVEMENT
	public function moveLeft():Void
	{
		flipX = true;
		acceleration.x -= PLAYER_SPEED_X;
	}

	public function moveRight():Void
	{
		flipX = false;
		acceleration.x += PLAYER_SPEED_X;
	}

	public function jump():Void
	{
    if (touchingFloor == true)
    {
  		y -= 1;
  		velocity.y = -PLAYER_JUMP;
    }
	}

  public function punch():Void
	{
    animation.play("attack");
    didAttack = true;
  }

  public function squish():Void
  {
    loadGraphic(Reg.IMG_PLAYER_DEATH_SPRITESHEET, 207, 151);
    var frames:Array<Int> = new Array<Int>();
    for (i in 0...64)
    {
      frames.push(i);
    }
    animation.add("squish", frames, 16, false);

    animation.play("squish");

    acceleration.y = 0;
    acceleration.x = 0;
    velocity.set(0,0);
    x -= 20;
    y -= 20;
    immovable = true;
    isDead = true;
  }
}
