package;

import flixel.util.FlxSave;

/**
 * Handy, pre-built Registry class that can be used to store
 * references to objects and other things for quick-access. Feel
 * free to simply ignore it or change it in any way you like.
 */
class Reg
{
	// Font
	public static inline var DEFAULT_FONT:String = "assets/images/visitor2.ttf";
	public static inline var DEFAULT_FONT_COLOUR:Int = 0x4aff2c;

	// Graphics
	public static inline var IMG_EYE_BACK:String = "assets/images/ritual_eye_back.png";
	public static inline var IMG_EYE_IRIS:String = "assets/images/ritual_eye_IRIS.png";
	public static inline var IMG_EYE_TOP:String = "assets/images/ritual_eye_top.png";

	public static inline var IMG_TITLE_TEXT:String = "assets/images/title_text.png";

	public static inline var IMG_PILLAR_1:String = "assets/images/pillar_1.png";
	public static inline var IMG_PILLAR_2:String = "assets/images/pillar_2.png";

	public static inline var IMG_CRATE_LARGE:String = "assets/images/crate_large.png";
	public static inline var IMG_CRATE_SMALL:String = "assets/images/crate_small.png";

	public static inline var IMG_PLATFORM3X1:String = "assets/images/Platform3x1.png";
	public static inline var IMG_PLATFORM5X1:String = "assets/images/Platform5x1.png";

	public static inline var IMG_PRESSURE_PLATE:String = "assets/images/pressure_plate_spritesheet64x10.png";

	public static inline var IMG_TEXT_SCREEN:String = "assets/images/text_screen.png";

	public static inline var IMG_PLAYER_SPRITESHEET:String = "assets/images/jeffspritesheet122x129.png";

	public static inline var IMG_DOOR_BACK_SPRITESHEET:String = "assets/images/doorbackspritesheet153x316.png";
	public static inline var IMG_DOOR_FRONT_SPRITESHEET:String = "assets/images/doorfrontspritesheet177x316.png";

	public static inline var IMG_ATTACK_EFFECT_SPRITESHEET:String = "assets/images/attack_effect_spritesheet120x20.png";

	public static inline var IMG_LASER_CHARGE_SPRITESHEET:String = "assets/images/eye_laser_charge100x100.png";

	public static inline var IMG_TARGET_DUMMY_SPRITESHEET:String = "assets/images/Dummy_sprite150x150.png";

	public static inline var IMG_PLAYER_DEATH_SPRITESHEET:String = "assets/images/dying_sprite207x151.png";

	public static inline var IMG_START_GAME_SCREEN:String = "assets/images/start_screen.png";
	public static inline var IMG_END_GAME_SCREEN:String = "assets/images/gameover_screen.png";
	public static inline var IMG_WIN_GAME_SCREEN:String = "assets/images/win_screen.png";

	public static inline var IMG_PIPES_STRING:String = "assets/images/big_pipe.png";

	public static inline var IMG_LIGHT:String = "assets/images/light_ani70x117.png";

	// Config
	public static inline var DEFAULT_GRAVITY:Int = 2020;
	public static inline var SCREEN_EXIT_PADDING:Int = 64;

	// Audio

	/**
	 * Generic levels Array that can be used for cross-state stuff.
	 * Example usage: Storing the levels of a platformer.
	 */
	public static var levels:Array<Dynamic> = [];
	/**
	 * Generic level variable that can be used for cross-state stuff.
	 * Example usage: Storing the current level number.
	 */
	public static var level:Int = 0;
	/**
	 * Generic scores Array that can be used for cross-state stuff.
	 * Example usage: Storing the scores for level.
	 */
	public static var scores:Array<Dynamic> = [];
	/**
	 * Generic score variable that can be used for cross-state stuff.
	 * Example usage: Storing the current score.
	 */
	public static var score:Int = 0;
	/**
	 * Generic bucket for storing different FlxSaves.
	 * Especially useful for setting up multiple save slots.
	 */
	public static var saves:Array<FlxSave> = [];
}
