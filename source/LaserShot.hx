package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class LaserShot extends FlxSprite
{
  public static inline var CHARGE_WIDTH:Int = 100;
  public static inline var CHARGE_HEIGHT:Int = 100;

  public var isFiringLaser:Bool = false;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_LASER_CHARGE_SPRITESHEET, true, CHARGE_WIDTH, CHARGE_HEIGHT);

    setSize(40, 40);
		/*offset.set(20, 20);*/

    animation.add("laser", [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0], 12, false);

    animation.play("laser");
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    if (animation.finished == true)
    {
      alpha = 0;
    }

    super.update();
	}

}
