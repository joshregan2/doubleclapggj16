package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class RitualEye extends FlxTypedSpriteGroup<FlxSprite>
{
  public static inline var EYE_WIDTH:Int = 113;
  public static inline var EYE_HEIGHT:Int = 113;
  public static inline var INNER_EYE_WIDTH:Int = 58;
  public static inline var INNER_EYE_HEIGHT:Int = 58;

  public var laserChargeSprites:Array<FlxSprite>;

  public var eyeSprite:FlxSprite;
  public var innerEyeSprite:FlxSprite;
  public var eyeCoverSprite:FlxSprite;

  private var eyeCenterX:Float;
  private var eyeCenterY:Float;

  public var eyeDirection:FlxVector;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    eyeSprite = new FlxSprite(0,0);
    eyeSprite.loadGraphic(Reg.IMG_EYE_BACK,EYE_WIDTH,EYE_HEIGHT);
    eyeSprite.scale.set(0.5, 0.5);
    add(eyeSprite);

    eyeCenterX = (EYE_WIDTH/2);
    eyeCenterY = (EYE_HEIGHT/2);

    innerEyeSprite = new FlxSprite(eyeCenterX-(INNER_EYE_WIDTH/2), eyeCenterY-(INNER_EYE_HEIGHT/2));
    innerEyeSprite.loadGraphic(Reg.IMG_EYE_IRIS, INNER_EYE_WIDTH, INNER_EYE_HEIGHT);
    innerEyeSprite.scale.set(0.5, 0.5);
    add(innerEyeSprite);

    eyeCoverSprite = new FlxSprite(0,0);
    eyeCoverSprite.loadGraphic(Reg.IMG_EYE_TOP,EYE_WIDTH,EYE_HEIGHT);
    eyeCoverSprite.scale.set(0.5, 0.5);
    add(eyeCoverSprite);

    var startX:Float = 16.0;
    var chargeSpacingX:Float = 10.0;
    var chargeSize:Float = 12.0;
    laserChargeSprites = new Array<FlxSprite>();
    for (i in 0...4)
    {
      var chargeSprite:FlxSprite = new FlxSprite(startX + (i * (chargeSpacingX+chargeSize)), 0);
      chargeSprite.makeGraphic(Std.int(chargeSize),Std.int(chargeSize), 0xffff1616);
      add(chargeSprite);
      laserChargeSprites.push(chargeSprite);
    }

    updateLaserCharges(0);
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{

    super.update();
	}

  public function lookAtPosition(posX:Float, posY:Float):Void
  {
    var direction:FlxVector = new FlxVector(posX - this.x, posY - this.y);
    direction = direction.normalize();

    var radius:Float = (EYE_HEIGHT/2) * 0.15;

    innerEyeSprite.x = this.x + eyeCenterX + ((direction.x * radius) - (INNER_EYE_WIDTH/2));
    innerEyeSprite.y = this.y + eyeCenterY + ((direction.y * radius) - (INNER_EYE_HEIGHT/2));

    eyeDirection = direction;
  }

  public function updateLaserCharges(charges:Int):Void
  {
    for (s in 0...laserChargeSprites.length)
    {
      laserChargeSprites[s].alpha = 0;
    }

    for (i in 0...charges)
    {
      laserChargeSprites[i].alpha = 1;
    }
  }
}
