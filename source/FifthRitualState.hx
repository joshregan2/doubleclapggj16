package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class FifthRitualState extends RitualState
{
	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{


		super.update();
	}

  override public function setupRitualScene():Void
  {
    backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    add(backgroundSprite);

    setupLevelTilemap("Room5.tmx", "Tileset.png");

    setupPlayerAtPosition(0, 0, true);

    setupRitualEyeAtPosition(512, 1360);

    super.setupRitualScene();
  }

	override public function resetRitualState():Void
	{
		player.x = 0;
  	player.y = 0;
	}

	override public function goNextRitualState(Void):Void
	{
    // TODO: Temp - should go to end game
		FlxG.switchState(new FifthRitualState());
	}
}
