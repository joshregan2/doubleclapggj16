package;

import flixel.FlxG;
import flixel.FlxBasic;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class RitualState extends BasePlayState
{
  private var backgroundSprite:FlxSprite;
  private var pipesSprite:FlxSprite;
  private var levelTileMap:FlxTilemap;
  private var player:Player;
  private var ritualEye:RitualEye;
  private var fadeOverlaySprite:FlxSprite;

  public var didSucceed:Bool;
  public var levelComplete:Bool;

  public var attackEffect:AttackEffect;

  public static inline var LASER_SPEED:Float = 400.0;
  public static inline var LASER_DELAY:Float = 1.0;
  public var laserChargeCount:Int = 0;
  public var laserCounter:Float = 0;
  public var laserShot:LaserShot;
  public var isFiringLaser:Bool = false;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();

		FlxG.updateFramerate = 124;

    didSucceed = false;
    levelComplete = false;

    setupRitualScene();

    transitionIn();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
    // Fire laser from eye at regular intervals
    if (laserCounter >= LASER_DELAY &&
        isFiringLaser == false &&
        laserShot == null &&
        player.isDead == false)
    {
      laserChargeCount++;
      laserCounter = 0;
    }
    else
    {
      laserCounter += FlxG.elapsed;
    }

    if (laserChargeCount > ritualEye.laserChargeSprites.length)
    {
      laserChargeCount = 0;
      ritualEye.updateLaserCharges(laserChargeCount);
      fireEyeLaserAtPlayer();
      isFiringLaser = true;
    }
    else
    {
      ritualEye.updateLaserCharges(laserChargeCount);
    }

    // Remove laser projectile after animation finishes
    if (laserShot != null &&
        laserShot.alpha == 0)
    {
      removeEyeLaserProjectile();
    }

    // Remove attack effect when animation finishes
    if (attackEffect != null &&
        attackEffect.alpha == 0)
    {
      remove(attackEffect);
      attackEffect.destroy();
      attackEffect = null;
    }

    // Check if eye laser hits player
    if (laserShot != null &&
        FlxG.collide(laserShot, player) == true)
    {
       // TODO: Player dies
       if (player.isDead == false)
       {
         player.squish();
         haxe.Timer.delay(restartTransition, 4000);
       }
       removeEyeLaserProjectile();
    }

    // Handle player controls
    handleInput();

    // Tilemap collision
    if (levelTileMap != null && player != null)
    {
      FlxG.collide(player, levelTileMap);

      if (player.isTouching(FlxObject.DOWN) == true)
      {
        player.touchingFloor = true;
      }
      else
      {
        player.touchingFloor = false;
      }
    }

    // Make eye look at player
    if (ritualEye != null && player != null)
    {
      ritualEye.lookAtPosition(player.x, player.y);
    }


    // Check for level completion
    checkDidBreakRitual();

		super.update();
	}

  // Configuration
  public function setupLevelTilemap(mapName:String, tileset:String):Void
 	{
		// Load the TMX data
    var tiledLevel:TiledMap = new TiledMap("assets/data/" + mapName);

    // Get map variables
    var tileSize = tiledLevel.tileWidth;

    var mapW = tiledLevel.width;
    var mapH = tiledLevel.height;

		// Creat actual tile map
		levelTileMap = new FlxTilemap();

		// Loop through each tile layer and render tile map
    for (layer in tiledLevel.layers)
    {
        var layerData:Array<Int> = layer.tileArray;

        var tilesheetName:String = tileset;
        var tilesheetPath:String = "assets/images/" + tilesheetName;

        levelTileMap.widthInTiles = mapW;
        levelTileMap.heightInTiles = mapH;

        var tileGID:Int = getStartGid(tiledLevel, tilesheetName);

        levelTileMap.loadMap(layer.tileArray, tilesheetPath, tileSize, tileSize, FlxTilemap.OFF, tileGID);
    }

		FlxG.camera.setBounds(0, 0, tiledLevel.fullWidth, tiledLevel.fullHeight, true);

		add(levelTileMap);
 	}

	public function getStartGid (tiledLevel:TiledMap, tilesheetName:String):Int
  {
    // This function gets the starting GID of a tilesheet

    // Note: "0" is empty tile, so default to a non-empty "1" value.
    var tileGID:Int = 1;

    for (tileset in tiledLevel.tilesets)
    {
        // We need to search the tileset's firstGID -- to do that,
        // we compare the tilesheet paths. If it matches, we
        // extract the firstGID value.
        var tilesheetPath:Path = new Path(tileset.imageSource);
        var thisTilesheetName = tilesheetPath.file + "." + tilesheetPath.ext;
        if (thisTilesheetName == tilesheetName)
        {
            tileGID = tileset.firstGID;
        }
    }

    return tileGID;
  }

  public function setupRitualScene():Void
  {

    fadeOverlaySprite = new FlxSprite(0,0);
    fadeOverlaySprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    fadeOverlaySprite.alpha = 0;
		add(fadeOverlaySprite);
  }

  public function setupPlayerAtPosition(x:Int, y:Int, cameraFollow:Bool):Void
  {
    player = new Player(x, y);
    add(player);

    if (cameraFollow == true)
    {
      FlxG.camera.follow(player, FlxCamera.STYLE_PLATFORMER, new FlxPoint(0,0), 0.4);
    }
  }

  public function setupRitualEyeAtPosition(x:Int, y:Int):Void
  {
    ritualEye = new RitualEye(x,y);
    add(ritualEye);
  }

  // Input Handling
  public function handleInput():Void
  {
    player.acceleration.x = 0;

    if (player.isDead == false)
    {
      // Handle player movement
      if (FlxG.keys.anyPressed(["LEFT", "A"]))
  		{
        player.moveLeft();
      }

      if (FlxG.keys.anyPressed(["RIGHT", "D"]))
  		{
        player.moveRight();
      }

      if (FlxG.keys.anyPressed(["UP", "W"]))
  		{
        player.jump();
      }

      if (FlxG.keys.anyPressed(["SPACE"]))
  		{
        if (player.didAttack == false && attackEffect == null)
        {
          player.punch();
          var effectOffsetX:Int = player.flipX == true ? -AttackEffect.ATTACK_EFFECT_WIDTH : Player.PLAYER_BOUNDS_WIDTH;
          var effectY:Int = Std.int(player.y) + 48;
          attackEffect = new AttackEffect(Std.int(player.x) + effectOffsetX, effectY);
          attackEffect.flipX = player.flipX;
          add(attackEffect);
        }
      }
    }
  }

  // Game Logic
  public function removeEyeLaserProjectile():Void
  {
    if (laserShot != null)
    {
      remove(laserShot);
      laserShot.destroy();
      laserShot = null;
    }
    isFiringLaser = false;
  }

  public function fireEyeLaserAtPlayer():Void
  {
    laserShot = new LaserShot(Std.int(ritualEye.x), Std.int(ritualEye.y));
    /*laserShot.acceleration.x = ritualEye.eyeDirection.x * LASER_SPEED;
    laserShot.acceleration.y = ritualEye.eyeDirection.y * LASER_SPEED;*/
    laserShot.velocity.set(ritualEye.eyeDirection.x * LASER_SPEED, ritualEye.eyeDirection.y * LASER_SPEED);
    add(laserShot);

    FlxG.camera.flash(0xffff1616,0.2, null, true);
  }

  public function checkIfPlayerAttackHitSprite(otherSprite:FlxBasic):Bool
  {
    if (attackEffect != null)
    {
      return FlxG.overlap(attackEffect, otherSprite);
    }
    else return false;
  }

  public function checkDidBreakRitual():Void
  {
    if (levelComplete == false)
    {
      // Check if they broke the RitualState
      if (didSucceed == true)
      {
        levelComplete = true;
        transitionOut();
      }
    }
  }

  public function resetRitualState():Void
  {
    // Override
  }

  public function transitionIn():Void
  {
      fadeOverlaySprite.alpha = 1;
      FlxSpriteUtil.fadeOut(fadeOverlaySprite, 1.0, false);
  }

  public function transitionOut():Void
  {
      FlxSpriteUtil.fadeIn(fadeOverlaySprite, 1.0, false, goNextRitualState);
  }

  public function goNextRitualState(Void):Void
  {
    // Override
  }

  public function restartTransition():Void
  {
      FlxSpriteUtil.fadeIn(fadeOverlaySprite, 1.0, false, restartWholeGame);
  }

  public function restartWholeGame(Void):Void
  {
    FlxG.switchState(new GameOverState());
  }

}
