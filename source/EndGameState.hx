package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;

/**
 * A FlxState which can be used for the game's menu.
 */
class EndGameState extends BasePlayState
{
  public var backgroundSprite:FlxSprite;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();

    backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.loadGraphic(Reg.IMG_WIN_GAME_SCREEN, FlxG.width, FlxG.height);
    add(backgroundSprite);
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
    if (FlxG.keys.anyPressed(["SPACE"]))
    {
      FlxG.switchState(new FirstRitualState());
    }
		super.update();
	}
}
