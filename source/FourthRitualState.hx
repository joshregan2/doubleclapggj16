package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class FourthRitualState extends RitualState
{
	private var hintTextScreen:TextScreenDisplay;
  private var targetDummy:TargetDummy;
  private var crate:MovableBlock;

	private var doorBack:BigDoorBack;
	private var doorFront:BigDoorFront;

	private var doorBlocker:FlxSprite;

	private var doorOpen:Bool = false;

  private var crateDidWarp:Bool = false;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
    if (player.x >= FlxG.width)
		{
			resetRitualState();
		}

		if (player.x <= 0)
		{
			player.x += FlxG.width;
			doorOpen = false;
			doorBack.close();
			doorFront.close();
			targetDummy.animation.play("idle");
		}

    if (crate.x >= FlxG.width)
		{
      crate.acceleration.x = 0;
      crate.x = 64*8;
      crate.y = 50;
      crateDidWarp = true;
		}
		FlxG.collide(doorBlocker, player);
    FlxG.collide(crate, levelTileMap);
    FlxG.collide(player, crate);
		FlxG.collide(player, targetDummy);

    if (checkIfPlayerAttackHitSprite(targetDummy) == true &&
				doorOpen == false)
		{
			targetDummy.animation.play("hit");
			doorOpen = true;
			doorBack.open();
			doorFront.open();
		}

		if (doorOpen == false)
		{
			FlxG.collide(doorFront, player);
		}

    if (FlxG.collide(crate, targetDummy) == true &&
        crateDidWarp == true)
    {
			targetDummy.animation.play("broken");
      didSucceed = true;
    }

		super.update();
	}

  override public function setupRitualScene():Void
  {
		backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    add(backgroundSprite);

    pipesSprite = new FlxSprite(0,0);
    pipesSprite.loadGraphic(Reg.IMG_PIPES_STRING, FlxG.width,FlxG.height);
    add(pipesSprite);

    setupLevelTilemap("Room4.tmx", "Tileset.png");

		var doorY:Int = (64*3) + 2;
		doorBlocker = new FlxSprite(64,doorY);
		doorBlocker.makeGraphic(BigDoorFront.DOOR_FRONT_WIDTH+100, 150, FlxColor.RED);
		doorBlocker.immovable = true;
		add(doorBlocker);

		doorBack = new BigDoorBack(64*3,doorY);
		doorBack.flipX = true;
		add(doorBack);
		doorBack.close();

    targetDummy = new TargetDummy(64*7,FlxG.height-((64*4)+TargetDummy.TARGET_DUMMY_HEIGHT));
		targetDummy.flipX = true;
    add(targetDummy);

    crate = new MovableBlock(64*12,64*7);
    add(crate);

    setupPlayerAtPosition(FlxG.width-Player.PLAYER_BOUNDS_WIDTH, FlxG.height-((64*4)+Player.PLAYER_BOUNDS_HEIGHT), false);

		doorFront = new BigDoorFront(64,doorY);
		doorFront.flipX = true;
		add(doorFront);
		doorFront.close();

    setupRitualEyeAtPosition(256, 64);

    super.setupRitualScene();
  }

	override public function resetRitualState():Void
	{
		player.x = 64*7;
  	player.y = -(Player.PLAYER_BOUNDS_HEIGHT/2);
	}

	override public function goNextRitualState(Void):Void
	{
		FlxG.switchState(new EndGameState());
	}
}
