package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class AnimatedLight extends FlxSprite
{
  public static inline var LIGHT_WIDTH:Int = 70;
  public static inline var LIGHT_HEIGHT:Int = 117;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_LIGHT, true, LIGHT_WIDTH, LIGHT_HEIGHT);
    immovable = true;
    
    var fRate:Int = 12;

    var idleFrames:Array<Int> = new Array<Int>();
    for (i in 0...12)
    {
      idleFrames.push(i);
    }
    animation.add("idle", idleFrames, fRate, true);
    animation.play("idle");
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    super.update();
	}
}
