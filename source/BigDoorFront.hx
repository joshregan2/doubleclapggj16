package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class BigDoorFront extends FlxSprite
{
  public static inline var DOOR_FRONT_WIDTH:Int = 177;
  public static inline var DOOR_FRONT_HEIGHT:Int = 316;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_DOOR_FRONT_SPRITESHEET,DOOR_FRONT_WIDTH,DOOR_FRONT_HEIGHT);

    animation.add("close", [2,1,0], false);
    animation.add("open", [0,1,2], false);

    immovable = true;
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{

    super.update();
	}

  public function open():Void
  {
    animation.play("open");
  }

  public function close():Void
  {
    animation.play("close");
  }
}
