package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.addons.editors.tiled.TiledMap;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.addons.ui.FlxUICursor;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class ThirdRitualState extends RitualState
{
	private var hintTextScreen:TextScreenDisplay;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
    if (player.x <= 0)
		{
			didSucceed = true;
		}

		super.update();
	}

  override public function setupRitualScene():Void
  {
		backgroundSprite = new FlxSprite(0,0);
    backgroundSprite.makeGraphic(FlxG.width,FlxG.height, FlxColor.BLACK);
    add(backgroundSprite);

    pipesSprite = new FlxSprite(0,0);
    pipesSprite.loadGraphic(Reg.IMG_PIPES_STRING, FlxG.width,FlxG.height);
    add(pipesSprite);

    setupLevelTilemap("Room3.tmx", "Tileset.png");

    setupPlayerAtPosition(7*64, -Player.PLAYER_BOUNDS_HEIGHT, true);

    setupRitualEyeAtPosition(512, 1360);

    super.setupRitualScene();
  }

	override public function resetRitualState():Void
	{

	}

	override public function goNextRitualState(Void):Void
	{
		FlxG.switchState(new FourthRitualState());
	}
}
