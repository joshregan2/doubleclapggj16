package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class MovableBlock extends FlxSprite
{
  public static inline var BLOCK_WIDTH:Int = 64;
  public static inline var BLOCK_HEIGHT:Int = 64;

  public static inline var BLOCK_DRAG:Int = 400;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    /*makeGraphic(BLOCK_WIDTH, BLOCK_HEIGHT, FlxColor.WHITE);*/
    loadGraphic(Reg.IMG_CRATE_SMALL, BLOCK_WIDTH, BLOCK_HEIGHT);

    drag.x = BLOCK_DRAG;
    acceleration.y = Reg.DEFAULT_GRAVITY;
    maxVelocity.set(100, 0);
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    super.update();
	}
}
