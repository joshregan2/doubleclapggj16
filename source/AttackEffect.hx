package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedSpriteGroup;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxVector;

class AttackEffect extends FlxSprite
{
  public static inline var ATTACK_EFFECT_WIDTH:Int = 120;
  public static inline var ATTACK_EFFECT_HEIGHT:Int = 20;

	public function new(X:Int, Y:Int)
	{
		super(X, Y);

    loadGraphic(Reg.IMG_ATTACK_EFFECT_SPRITESHEET,ATTACK_EFFECT_WIDTH,ATTACK_EFFECT_HEIGHT);

    animation.add("attack", [0,1,2,3], false);

    immovable = true;

    animation.play("attack");
	}

	override public function destroy():Void
	{
		super.destroy();

	}

	override public function update():Void
	{
    super.update();

    if (animation.finished == true)
    {
      alpha = 0;
    }
	}
}
